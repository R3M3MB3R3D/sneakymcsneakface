﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MasterControl 
{
	protected override void Start () 
	{
		base.Start ();
	}
	protected override void Update()
	{
		inputHandler ();
		pawn.updateHealth ();
	}

	void inputHandler()
	{
		if (Input.GetButton ("MoveForward")) 
		{
			moveDirection = 1.0f;
			pawn.move (moveDirection);
		}
		if (Input.GetButton ("MoveBackward")) 
		{
			moveDirection = -1;
			pawn.move (moveDirection);
		}
		if (Input.GetButton ("MoveLeft")) 
		{
			rotateDirection = 1;
			pawn.rotate (rotateDirection);
		}
		if (Input.GetButton ("MoveRight")) 
		{
			rotateDirection = -1;
			pawn.rotate (rotateDirection);
		}
	}
}