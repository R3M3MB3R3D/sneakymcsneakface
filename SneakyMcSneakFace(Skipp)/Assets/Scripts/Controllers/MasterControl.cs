﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MasterControl : MonoBehaviour 
{

	protected MasterPawn pawn;

	protected float moveDirection;
	protected float rotateDirection;

	public enum AIStates
	{
		idle,
		alert,
		reset,
		attack
	};

	[SerializeField]
	public AIStates currentState;

	protected virtual void Start ()
	{
		pawn = GetComponent<MasterPawn> ();			
	}

	protected virtual void Update()
	{
		switch (currentState) 
		{
		case AIStates.idle:
			pawn.stateIdle ();
			break;
		case AIStates.alert:
			pawn.stateAlert ();
			break;
		case AIStates.reset:
			pawn.stateReset ();
			break;
		case AIStates.attack:
			pawn.stateAttack ();
			break;
		}
		pawn.updateHealth ();
	}
}