﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MasterControl 
{
	//Setting state to Idle, then using base operations for behavior
	protected override void Start () 
	{
		currentState = AIStates.idle;
		base.Start ();
	}
	protected override void Update()
	{
		base.Update();
	}
}