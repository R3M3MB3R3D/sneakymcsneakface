﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour 
{
	private void OnCollisionEnter2d (Collider2D other)
	{
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name == "Player") 
		{
			GameManager.instance.sceneProgress.LoadCredits ();
		}
	}
}