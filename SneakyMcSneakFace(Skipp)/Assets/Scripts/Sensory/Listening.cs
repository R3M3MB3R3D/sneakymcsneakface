﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Listening : MonoBehaviour 
{
	public NoiseMaker targetNoiseMaker;
	//A function for determining whether or not the Enemy heard the Player
	public bool listeningForNoise()
	{
		if (targetNoiseMaker == null) 
		{
			return false;
		} 
		else 
		{
			float noiseLevel = targetNoiseMaker.noiseLevel;
			float distance = Vector3.Distance (transform.position, targetNoiseMaker.gameObject.transform.position);
			noiseLevel -= distance;
			if (noiseLevel > 0) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
		}
	}
}