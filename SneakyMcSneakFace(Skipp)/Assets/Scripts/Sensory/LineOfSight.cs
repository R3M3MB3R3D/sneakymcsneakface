﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineOfSight : MonoBehaviour 
{
	//Creating variables to provide the AI with sight
	public float viewDistance;
	[Range(0,360)]
	public float fieldOfView;

	//Creating a variable for the AI to find
	public GameObject target;

	//Function for determining if the AI 'saw' the Player
	public bool inLineOfSight()
	{
		Vector3 vectorToTarget = target.transform.position - transform.position;
		RaycastHit2D hitInfo = Physics2D.Raycast (transform.position, vectorToTarget, viewDistance);
		Hitbox targetCollider = target.GetComponent<Hitbox> ();
		float angle = Vector3.Angle (vectorToTarget, transform.up / 2);

		//basically just checking to see if the hitbox is in range
		if (targetCollider == null) 
		{
			return false;
		}
		if (hitInfo.collider == null) 
		{
			return false;
		}
		if (angle <= fieldOfView) 
		{
			if (Vector3.Distance (transform.position, target.transform.position) < viewDistance) 
			{
				if (hitInfo.collider.name == targetCollider.name) 
				{
					return true;
				}
			}
		}
		return false;
	}
}