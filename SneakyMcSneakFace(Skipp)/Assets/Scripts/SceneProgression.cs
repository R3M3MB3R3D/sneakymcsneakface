﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneProgression : MonoBehaviour 
{
	//Function for moving to Title Screen
	public void LoadMenu()
	{
		Debug.Log ("Loading Menu");
		SceneManager.LoadScene (0);
	}

	//Function for moving to game play
	public void LoadGame()
	{
		Debug.Log ("Loading Game");
		SceneManager.LoadScene (1);
	}

	//Function for moving to Credits
	public void LoadCredits()
	{
		Debug.Log ("Loading Credits");
		SceneManager.LoadScene (2);
	}

	//Function for exiting the game
	public void ExitGame()
	{
		Debug.Log ("Closing Application");
		Application.Quit ();
	}
}

//Goto - Build Settings with your scenes pre-made 
//and drag them into the "Scenes to Build" Section at the top
//Order the scenes and then Exit the settings menu
//Select the proper function from the drop down lists on the relevant buttons