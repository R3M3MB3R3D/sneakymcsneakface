﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPawn : MasterPawn 
{
	public Slider healthBar;
	public SceneProgression sceneProgression;

	public void Awake()
	{}

	public override void Start()
	{
		//!!! Null Reference Exception - not set to an instance ?
		GameManager.instance.player = this.gameObject;
		currentHealth = maxHealth;
		healthBar.value = updateHealth();
		sceneProgression = GetComponent<SceneProgression> ();

		base.Start ();
	}

	//Player doesn't need AI control, so I'll override them
	public override void stateIdle()
	{}
	public override void stateAlert()
	{}
	public override void stateReset()
	{}
	public override void stateAttack()
	{}
	public override void attack()
	{}
	public override void dealDamage()
	{}
	public override void updateTarget()
	{}
	public override void move()
	{}
	public override void rotate()
	{}

	public override void move (float moveDirection)
	{
		noiseMaker.noiseLevel = 2;
		transform.Translate (0, moveDirection * Time.deltaTime * moveSpeed, 0);
	}
	public override void rotate(float rotateDirection)
	{
		noiseMaker.noiseLevel = 1;
		transform.Rotate (0, 0, rotateDirection * Time.deltaTime * rotateSpeed);
	}
	public override void takeDamage(int damageToTake)
	{
		currentHealth -= damageToTake;
		healthBar.value = updateHealth ();

		if (currentHealth <= 0)
		{
			sceneProgression.LoadCredits();
		}
	}
	public override float updateHealth()
	{
		return currentHealth / maxHealth;
	}
}