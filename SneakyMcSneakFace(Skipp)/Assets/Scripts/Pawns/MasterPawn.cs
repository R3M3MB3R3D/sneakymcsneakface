﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MasterPawn : MonoBehaviour 
{
	[SerializeField] protected int moveSpeed;
	[SerializeField] protected int rotateSpeed;
	[SerializeField] protected Vector3 spawnPos;
	[SerializeField] protected float attackRange;
	[SerializeField] protected float attackCoolDown;
	[SerializeField] protected int damage;
	[SerializeField] protected float currentHealth;
	[SerializeField] protected float maxHealth;
	protected float attackCoolDownCurrent;

	protected MasterControl controller;

	[SerializeField] protected Transform target;
	protected Vector3 targetPos;

	protected LineOfSight sight;
	protected Listening listen;
	protected NoiseMaker noiseMaker;
	protected Hitbox hitbox;

	public virtual void Start()
	{
		spawnPos = transform.position;
		targetPos = spawnPos;

		controller = GetComponent<MasterControl> ();

		sight = GetComponent<LineOfSight> ();
		listen = GetComponent<Listening> ();
		noiseMaker = GetComponent<NoiseMaker> ();
		hitbox = GetComponent<Hitbox> ();
	}

	public virtual void stateIdle()
	{
		if (transform.position != spawnPos) 
		{
			// !!! Null Reference Exception - not set to an instance ?
			controller.currentState = MasterControl.AIStates.reset;
		}
		if (sight.inLineOfSight ()) 
		{
			targetPos = sight.target.transform.position;
			controller.currentState = MasterControl.AIStates.alert;
		}
		if (listen.listeningForNoise ()) 
		{
			targetPos = listen.targetNoiseMaker.gameObject.transform.position;
			controller.currentState = MasterControl.AIStates.alert;
		}
	}
	public virtual void stateAlert()
	{
		move ();
		rotate ();
		updateTarget ();

		if (sight.inLineOfSight () == false && listen.listeningForNoise () == false) 
		{
			controller.currentState = MasterControl.AIStates.idle;
		}
		if (Vector3.Distance (targetPos, transform.position) < attackRange) 
		{
			controller.currentState = MasterControl.AIStates.attack;
		}
	}
	public virtual void stateReset()
	{
		targetPos = spawnPos;
		move ();
		rotate ();
		if (transform.position == spawnPos) 
		{
			controller.currentState = MasterControl.AIStates.idle;
		}
	}
	public virtual void stateAttack()
	{
		if (attackCoolDownCurrent <= 0) 
		{
			attack ();
			updateTarget ();
		}
		attackCoolDownCurrent -= Time.deltaTime;
		if (Vector3.Distance (targetPos, transform.position) > attackRange) 
		{
			controller.currentState = MasterControl.AIStates.alert;
		}
	}
	public virtual void move()
	{
		transform.position = Vector2.MoveTowards (transform.position, targetPos, moveSpeed * Time.deltaTime);
		noiseMaker.noiseLevel = 2.0f;
	}
	public virtual void move (float moveDirection)
	{
		//An overload meant for use by the player
	}
	public virtual void rotate()
	{
		Vector3 direction = targetPos - transform.position;
		direction.Normalize ();
		float zAngle = (Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg - 90);
		Quaternion targetLocation = Quaternion.Euler (0, 0, zAngle);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, targetLocation, rotateSpeed * Time.deltaTime);
	}
	public virtual void rotate (float rotateDirection)
	{
		//An overload meant for use by the player
	}
	public virtual void attack()
	{
		//for base pawn attack simply makes noise and does nothing else
		noiseMaker.noiseLevel = 5;
	}

	public virtual void updateTarget()
	{
		targetPos = target.transform.position;
	}

	public virtual float updateHealth()
	{
		return currentHealth;
	}
	public virtual void dealDamage()
	{
		target.gameObject.GetComponent<MasterPawn> ().takeDamage (damage);
	}
	public virtual void takeDamage (int damageToTake)
	{
		currentHealth -= damageToTake;
	}
}