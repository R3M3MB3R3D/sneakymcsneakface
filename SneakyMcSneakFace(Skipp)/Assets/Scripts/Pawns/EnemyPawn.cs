﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawn : MasterPawn
{

	public override	void Start () 
	{
		GameManager.instance.enemies.Add (this.gameObject);
		//Set Enemy Health here at Start
		//!!! Null Reference Exception - not set to an instance ?
		currentHealth = maxHealth;

		base.Start ();
	}
	public override void stateIdle()
	{
		base.stateIdle ();
	}
	public override void stateAlert()
	{
		base.stateAlert ();
	}
	public override void stateReset()
	{
		base.stateReset ();
	}
	public override void stateAttack()
	{
		base.stateAttack ();
	}
	public override void move()
	{
		base.move ();
	}
	public override void rotate()
	{
		base.rotate ();
	}
	public override void attack ()
	{
		attackCoolDownCurrent = attackCoolDown;
		noiseMaker.noiseLevel = 4;
		dealDamage ();
	}
	public override void dealDamage()
	{
		base.dealDamage ();
	}
	public override void updateTarget ()
	{
		targetPos = target.transform.position;
	}
	public override float updateHealth()
	{
		return currentHealth;
	}
	public override void takeDamage (int damageToTake)
	{
		currentHealth -= damageToTake;
	}
}