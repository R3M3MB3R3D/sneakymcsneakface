﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	//Creating an instance of the GameManager
	public static GameManager instance;
	//Linking Scenes to the GameManager
	public SceneProgression sceneProgress;

	//Referencing Assets 
	public GameObject player;
	public List<GameObject> enemies;

	//creating a singleton
	void Awake()
	{
		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else 
		{
			Destroy (this.gameObject);
		}
		sceneProgress = GetComponent<SceneProgression> ();
	}

	private void Start () 
	{
		sceneProgress.LoadMenu ();
	}

	void Update () 
	{
		
	}
}